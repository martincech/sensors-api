FROM microsoft/dotnet:2.1-aspnetcore-runtime

ARG VERSION=0.1

ENV ENVIRONMENT=Production
ENV VERSION=${VERSION}

ENV KAFKA=kafka:9092

ENV INFLUX=influx:8086
ENV INFLUX_HEARTBEAT_DB=_heartbeat

ENV IAM_ID=AKIAICJEM3DRAQ3ZPE3A
ENV IAM_SECRET=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
ENV COGNITO_ISSUER=https://cognito-idp.eu-central-1.amazonaws.com/eu-central-1_271lLNsx2

ENV SENTRY_DSN=https://220a147c104847c7826436a62d159b72@sentry.io/1380908

ENV ASPNETCORE_ENVIRONMENT=${ENVIRONMENT}
ENV ASPNETCORE_URLS "http://*:80/"

WORKDIR /app
VOLUME /app/logs
COPY SensorsApi/Production/ .
COPY startup.sh .
RUN \
   apt update \
   && apt -y install gettext-base   
RUN for f in *.json; do cp "$f" "$f.in"; done \
    && for f in *.config; do cp "$f" "$f.in"; done 
RUN chmod a+x startup.sh

EXPOSE 80
ENTRYPOINT ["./startup.sh"]