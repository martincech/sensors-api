﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Heartbeat;
using InfluxData.Net.Common.Enums;
using InfluxData.Net.InfluxDb;
using InfluxData.Net.InfluxDb.Models;
using NLog;
using SensorsApi.Models;

namespace SensorsApi.Influx
{
    public interface IInfluxClient
    {
        Task SaveUserInfoAsync(UserInfo userInfo);
        Task CreateDatabaseAsync();
    }

    public class InfluxClient : IInfluxClient
    {
        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        readonly IInfluxDbClient dbClient;
        readonly string heartbeatDatabase;

        public InfluxClient(HeartbeatSettings settings)
            : this(new InfluxDbClient(settings.Influx, "", "", InfluxDbVersion.v_1_3), settings.InfluxDatabase)
        {
        }

        public InfluxClient(IInfluxDbClient dbClient, string heartbeatDatabase)
        {
            this.dbClient = dbClient ?? throw new ArgumentNullException($"{nameof(dbClient)} cannot be null");
            this.heartbeatDatabase = !string.IsNullOrEmpty(heartbeatDatabase)
                ? heartbeatDatabase
                : throw new ArgumentException($"{nameof(heartbeatDatabase)} cannot be null or empty");
        }

        public async Task SaveUserInfoAsync(UserInfo userInfo)
        {
            try
            {
                if (userInfo != null)
                {
                    logger.Debug("Send user informations(heartbeat) to influx.");
                    var result = await dbClient.Client.WriteAsync(MapPoint(userInfo), heartbeatDatabase).ConfigureAwait(false);
                    logger.Debug($"Inlfux response: {result.StatusCode.ToString("d")} - {result.StatusCode}.");
                }
            }
            catch (Exception e)
            {
                logger.Error(e, "Failed to send user information(heartbeat) to influx.");
            }
        }

        public async Task CreateDatabaseAsync()
        {
            try
            {
                await dbClient.Database.CreateDatabaseAsync(heartbeatDatabase).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }

        static Point MapPoint(UserInfo userInfo)
        {
            return new Point
            {
                Name = nameof(UserInfo),
                Fields = new Dictionary<string, object>
                {
                    { "AppId", userInfo.App?.Id },                    
                },
                Tags = new Dictionary<string, object>
                {
                    { "AppVersion", userInfo.App?.Version },
                    { "AppLocalization", userInfo.App?.Localization },
                    { "OsName", userInfo.OS?.Name },
                    { "OsEdition", userInfo.OS?.Edition },
                    { "OsVersion", userInfo.OS?.Version },
                    { "OsLocalization", userInfo.OS?.Localization },                    
                }
            };
        }
    }
}
