﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SensorsApi.Extensions;
using SensorsApi.Filters;
using SensorsApi.Models;
using Veit.Kafka;

namespace SensorsApi.Controllers
{
    [Route("v1/[controller]")]
    [ServiceFilter(typeof(IPAddressFilterAttribute))]
    public class WebhooksController : Controller
    {
        private readonly IKafkaSender kafkaSender;
        static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public WebhooksController(IKafkaSender kafkaSender)
        {
            this.kafkaSender = kafkaSender;
        }

        /// <summary>
        /// Process text message from SMS gateway
        /// </summary>
        /// <param name="sms">Inbound SMS message</param>
        /// <response code="200">Success</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="500">Unexpected Error (typically 500)</response>
        [HttpPost("inbound-sms")]
        public IActionResult InboundSms([FromBody] InboundSms sms)
        {
            logger.Debug($"Inbound SMS : {JsonConvert.SerializeObject(sms, Formatting.None)}");

            if (sms.Map(out var data))
            {
                if (!kafkaSender.Send(data))
                {
                    logger.Error("Failed to send weight statistics to kafka");
                    return StatusCode((int)HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                logger.Error($"Invalid SMS format from phone number +{sms?.Msisdn} : \"{sms?.Text}\"");
            }

            return Ok();
        }

        [HttpPost("delivery-receipt")]
        public IActionResult DeliveryReceipt([FromBody] DeliveryReceipt receipt)
        {
            logger.Debug($"Delivery receipt : {JsonConvert.SerializeObject(receipt)}");
            return Ok();
        }
    }
}
