﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heartbeat;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SensorsApi.Influx;
using SensorsApi.Models;
using Veit.Diagnostics.AWS;

namespace SensorsApi.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class HeartbeatController : ControllerBase
    {
        readonly HeartbeatSettings settings;
        readonly S3File s3Storage;
        readonly IInfluxClient influxClient;

        public HeartbeatController(S3File s3Storage, HeartbeatSettings settings, IInfluxClient influxClient)
        {
            this.s3Storage = s3Storage;
            this.settings = settings;
            this.influxClient = influxClient;
        }

        [HttpPost("/v1/heartbeatbackup")] //temporary for update collector v0.7.4XX
        [HttpPost("")]
        public async Task<IActionResult> PostAsync([FromBody]UserInfo info)
        {
            await influxClient.SaveUserInfoAsync(info).ConfigureAwait(false);
            var etag = Request.Headers.FirstOrDefault(f => f.Key == "ETag").Value;
            var result = await s3Storage.ReadAsync(settings.Bucket, settings.File, etag.FirstOrDefault()).ConfigureAwait(false);

            if (result.Value == null)
            {
                return StatusCode(StatusCodes.Status304NotModified);
            }
            HttpContext.Response.Headers.Add("ETag", result.Key);
            return new FileContentResult(Encoding.UTF8.GetBytes(result.Value), "application/json");
        }
    }
}
