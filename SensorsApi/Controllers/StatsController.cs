﻿using System;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SensorsApi.Extensions;
using Veit.Bat.Common.Schema;
using Veit.Kafka;

namespace SensorsApi.Controllers
{
    [Authorize]
    [Route("v1/[controller]")]
    public class StatsController : Controller
    {
        static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        readonly IKafkaSender kafkaSender;

        public StatsController(IKafkaSender kafkaSender)
        {
            this.kafkaSender = kafkaSender;
        }

        /// <summary>
        /// Send statistic data
        /// </summary>
        /// <remarks>&#39;Send statistic data&#39;</remarks>
        /// <param name="data"></param>
        /// <response code="200">Success</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="412">Precondition Failed, Required Data Constraints Not Met</response>
        /// <response code="0">Unexpected Error (typically 500)</response>
        [HttpPost]
        public IActionResult Post([FromBody] StatsBody data)
        {
            logger.Debug("Recieved statistic samples");
            if (data == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            try
            {
                if (kafkaSender.Send(data.Map()))
                {
                    return Ok();
                }
            }
            catch (Exception e)
            {
                logger.Error(e, "Failed to send statistic data to kafka");
            }
            return StatusCode((int)HttpStatusCode.InternalServerError);
        }
    }
}
