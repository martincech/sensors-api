﻿using System.Collections.Generic;

namespace SensorsApi.Filters
{
    public class IPWhitelistConfiguration : IIPWhitelistConfiguration
    {
        public IEnumerable<string> IPAddresses { get; set; }
    }
}
