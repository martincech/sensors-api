﻿using System.Collections.Generic;

namespace SensorsApi.Filters
{
    public interface IIPWhitelistConfiguration
    {
        IEnumerable<string> IPAddresses { get; }
    }
}
