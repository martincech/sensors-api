﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using NetTools;

namespace SensorsApi.Filters
{
    public sealed class IPAddressFilterAttribute : ActionFilterAttribute
    {
        readonly IEnumerable<IPAddressRange> authorizedRanges;

        public IPAddressFilterAttribute(IIPWhitelistConfiguration configuration)
        {
            authorizedRanges = configuration.IPAddresses == null
                ? new List<IPAddressRange>()
                : configuration.IPAddresses.Select(item => IPAddressRange.Parse(item));
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var clientIPAddress = context.HttpContext.Connection.RemoteIpAddress;
            if (!authorizedRanges.Any(range => range.Contains(clientIPAddress)))
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
}
