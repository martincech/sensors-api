﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using NLog;
using NLog.Web;

namespace SensorsApi
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var configuringFileName = "nlog.config";
            var aspnetEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (string.IsNullOrEmpty(aspnetEnvironment))
            {
                aspnetEnvironment = "Production";
            }

            var environmentSpecificLogFileName = $"nlog.{aspnetEnvironment}.config";

            if (File.Exists(environmentSpecificLogFileName))
            {
                configuringFileName = environmentSpecificLogFileName;
            }
            var logger = NLogBuilder.ConfigureNLog(configuringFileName).GetCurrentClassLogger();
            try
            {
                logger.Debug($"Starting application with ASPNETCORE_ENVIRONMENT: {aspnetEnvironment}");
                CreateWebHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                LogManager.Shutdown();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
