﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace SensorsApi.Models
{
    [JsonObject(Description = "Delivery receipt callback from Nexmo.", Id = "delivery-receipt")]
    public class InboundSms
    {
        [JsonProperty("msisdn", Required = Required.Always)]
        [Description("The phone number that this inbound message was sent from.")]
        public string Msisdn { get; set; }

        [JsonProperty("to", Required = Required.Always)]
        [Description("The phone number the message was sent to.")]
        public string To { get; set; }

        [JsonProperty("messageId", Required = Required.Always)]
        [Description("The ID of the message.")]
        public string MessageId { get; set; }

        [JsonProperty("text", Required = Required.Always)]
        [Description("The message body for this inbound message.")]
        public string Text { get; set; }

        [JsonProperty("type", Required = Required.Always)]
        [Description("Message type (text, unicode, binary).")]
        public string Type { get; set; }

        [JsonProperty("keyword", Required = Required.Always)]
        [Description("The first word in the message body. This is typically used with short codes.")]
        public string Keyword { get; set; }

        [JsonProperty("message-timestamp", Required = Required.Always)]
        [Description("The time when Nexmo started to push this Delivery Receipt to your webhook endpoint.")]
        public string MessageTimestamp { get; set; }

        [JsonProperty("timestamp", Required = Required.Default)]
        [Description("A unix timestamp representation of message-timestamp.")]
        public string Timestamp { get; set; }

        [JsonProperty("nonce", Required = Required.Default)]
        [Description("A random string that forms part of the signed set of parameters, it adds an extra element of unpredictability into the signature for the request. ")]
        public string Nonce { get; set; }

        [JsonProperty("concat", Required = Required.Default)]
        [Description("True - if this is a concatenated message.")]
        public string Concat { get; set; }

        [JsonProperty("concat-ref", Required = Required.Default)]
        [Description("The transaction reference. All parts of this message share this value.")]
        public string ConcatRef { get; set; }

        [JsonProperty("concat-total", Required = Required.Default)]
        [Description("The number of parts in this concatenated message.")]
        public string ConcatTotal { get; set; }

        [JsonProperty("concat-part", Required = Required.Default)]
        [Description("The number of this part in the message. Counting starts at 1.")]
        public string ConcatPart { get; set; }

        [JsonProperty("data", Required = Required.Default)]
        [Description("The content of this message, if type is binary.")]
        public string Data { get; set; }

        [JsonProperty("udh", Required = Required.Default)]
        [Description("The hex encoded User Data Header, if type is binary")]
        public string Uhd { get; set; }
    }
}
