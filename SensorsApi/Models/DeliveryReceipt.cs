﻿using Newtonsoft.Json;
using System.ComponentModel;

namespace SensorsApi.Models
{
    [JsonObject(Description = "Delivery receipt callback from Nexmo.", Id = "delivery-receipt")]
    public class DeliveryReceipt
    {
        [JsonProperty("msisdn", Required = Required.Always)]
        [Description("The number the message was sent to.")]
        public string Msisdn { get; set; }

        [JsonProperty("to")]
        [Description("The SenderID you set in from in your request.")]
        public string To { get; set; }

        [JsonProperty("network-code", Required = Required.Always)]
        [Description("The Mobile Country Code Mobile Network Code (MCCMNC) of the carrier this phone number is registered with.")]
        public string NetworkCode { get; set; }

        [JsonProperty("messageId", Required = Required.Always)]
        [Description("The Nexmo ID for this message.")]
        public string MessageId { get; set; }

        [JsonProperty("price", Required = Required.Always)]
        [Description("The cost of the message.")]
        public string Price { get; set; }

        [JsonProperty("status", Required = Required.Always)]
        [Description("A code that explains where the message is in the delivery process.")]
        public string Status { get; set; }

        [JsonProperty("scts", Required = Required.Always)]
        [Description("When the DLR was recieved from the carrier in the following format YYMMDDHHMM.")]
        public string SCTS { get; set; }

        [JsonProperty("err-code", Required = Required.Always)]
        [Description("The status of the request. Will be a non 0 value if there has been an error.")]
        public string ErrCode { get; set; }

        [JsonProperty("message-timestamp", Required = Required.Always)]
        [Description("The time when Nexmo started to push this Delivery Receipt to your webhook endpoint.")]
        public string MessageTimestamp { get; set; }

    }
}
