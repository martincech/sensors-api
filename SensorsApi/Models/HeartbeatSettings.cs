﻿namespace SensorsApi.Models
{
    public class HeartbeatSettings
    {
        public string Bucket { get; set; }
        public string File { get; set; }
        public string Region { get; set; }
        public string IAMId { get; set; }
        public string IAMSecret { get; set; }
        public string Influx { get; set; }
        public string InfluxDatabase { get; set; }
    }
}
