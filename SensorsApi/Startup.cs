﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using SensorsApi.Extensions;
using SensorsApi.Filters;
using SensorsApi.Influx;
using Swashbuckle.AspNetCore.Swagger;
using Veit.Diagnostics.AWS;
using Veit.Kafka;

namespace SensorsApi
{
    public class Startup
    {
        IConfiguration Configuration { get; }
        IHostingEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter(new CamelCaseNamingStrategy()));
                    options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "BAT DATA API", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                var issuer = Configuration.TokenIssuer();
                var jwk = Veit.JWT.Configuration.JsonWebKeys.Get(issuer).Last();
                options.TokenValidationParameters = jwk.Value.TokenValidationParameters;
                options.Events = new JwtBearerEvents
                {
                    OnTokenValidated = ctx =>
                    {
                        if (!ctx.Principal.Claims.HasValidScope(Configuration.TokenScopes()))
                        {
                            ctx.Fail($"Failed to authorize user.");
                        }
                        return Task.FromResult(0);
                    }
                };
            });

            // Configure IPAddressFilterAttribute
            services.AddSingleton<IIPWhitelistConfiguration>(Configuration.GetIPWhitelist());
            services.AddScoped<IPAddressFilterAttribute, IPAddressFilterAttribute>();

            var heartbeatSettings = Configuration.GetHeartbeatSettings();
            services.AddSingleton(heartbeatSettings);
            services.AddScoped<IInfluxClient, InfluxClient>();
            services.AddScoped(c => new S3File(heartbeatSettings.IAMId, heartbeatSettings.IAMSecret, heartbeatSettings.Region));

            // Configure kafka
            #pragma warning disable CC0022 // Should dispose object
            services.AddSingleton<IKafkaSender>(new KafkaSender(Configuration.GetKafkaBroker(), GetKafkaCacheFile()));
            #pragma warning restore CC0022 // Should dispose object
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            app.UseSwagger();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseAuthentication();

            app.UseSwaggerUI(c =>
            {
                if (string.IsNullOrEmpty(Configuration.SwaggerUIFile()))
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "BAT DATA API v1");
                }
                else
                {
                    c.SwaggerEndpoint($"/{Configuration.SwaggerUIFile()}", "BAT DATA API v1");
                }
            });

            var option = new RewriteOptions();
            option.AddRedirect("^$", "swagger");
            app.UseRewriter(option);

            app.UseMvc().UseStaticFiles();

            serviceProvider.InitInfluxDatabase();
        }

        string GetKafkaCacheFile()
        {
            return Path.Combine(Environment.ContentRootPath, "Cache", "API.kafkaCache");
        }
    }
}
