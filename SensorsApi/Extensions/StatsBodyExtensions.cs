﻿using System.Collections.Generic;
using Veit.Bat.Common.Schema;
using Veit.Kafka.Topics;

namespace SensorsApi.Extensions
{
    public static class StatsBodyExtensions
    {
        public static string SexMale => "male";
        public static string SexFemale => "female";
        public static string SexUndefined => "undefined";

        public static List<DailyWeight> Map(this StatsBody stats)
        {
            var dailyWeights = new List<DailyWeight>();
            if (stats != null && stats.Scales != null)
            {
                foreach (var scale in stats.Scales)
                {
                    dailyWeights.AddRange(Map(scale));
                }
            }
            return dailyWeights;
        }

        static List<DailyWeight> Map(Scale scale)
        {
            var dailyWeights = new List<DailyWeight>();
            if (scale != null && scale.Flock != null)
            {
                var isMixed = scale.Flock.Males != null && scale.Flock.Females != null;
                if (scale.Flock.Males != null)
                    dailyWeights.AddRange(Map(scale.Flock.Males, scale.Id, isMixed ? SexMale : SexUndefined));
                if (scale.Flock.Females != null)
                    dailyWeights.AddRange(Map(scale.Flock.Females, scale.Id, isMixed ? SexFemale : SexUndefined));
            }
            return dailyWeights;
        }

        static List<DailyWeight> Map(SexStats sexStats, System.Guid uid, string sex)
        {
            var dailyWeights = new List<DailyWeight>();
            if (sexStats != null && sexStats.Stats != null)
            {
                foreach (var stat in sexStats.Stats)
                {
                    var dailyWeight = Map(stat);
                    if (dailyWeight != null)
                    {
                        dailyWeight.Uid = uid.ToString();
                        dailyWeight.Sex = sex;
                        dailyWeights.Add(dailyWeight);
                    }
                }
            }
            return dailyWeights;
        }

        static DailyWeight Map(DailyStats sample)
        {
            if (sample == null) return null;
            return new DailyWeight
            {
                Average = sample.Avg,
                Count = sample.Count,
                Cv = sample.Cv,
                Day = sample.Day,
                Gain = sample.Gain,
                Sigma = sample.Sig,
                Uniformity = sample.Uniformity,
                TimeStamp = sample.DateTime,
            };
        }
    }
}
