﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace SensorsApi.Extensions
{
    public static class ClaimValidationExtensions
    {
        public static bool HasValidScope(this IEnumerable<Claim> claims, string requiredScopes)
        {
            if (string.IsNullOrEmpty(requiredScopes)) return true;
            if (claims == null || !claims.Any(a => a.Type == "scope")) return false;
            var scopesClaim = claims.First(f => f.Type == "scope");
            return !GetScopes(requiredScopes).Except(GetScopes(scopesClaim.Value)).Any();
        }

        static string[] GetScopes(string scopes)
        {
            if (string.IsNullOrEmpty(scopes)) return Array.Empty<string>();
            return scopes.ToUpperInvariant().Split(" ");
        }
    }
}
