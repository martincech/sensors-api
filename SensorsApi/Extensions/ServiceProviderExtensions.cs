﻿using System;
using SensorsApi.Influx;

namespace SensorsApi.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static void InitInfluxDatabase(this IServiceProvider serviceProvider)
        {
            var influxClient = (IInfluxClient)serviceProvider.GetService(typeof(IInfluxClient));

            influxClient
                .CreateDatabaseAsync()
                .GetAwaiter()
                .GetResult();
        }
    }
}
