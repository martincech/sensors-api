﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using SensorsApi.Models;
using Veit.Kafka.Topics;

namespace SensorsApi.Extensions
{
    public static class InboundSmsExtensions
    {
        public const string SexMale = "male";
        public const string SexFemale = "female";
        public const string SexUndefined = "undefined";

        const int SHORT_MESSAGE_LENGHT = 18;

        public static bool Map(this InboundSms sms, out List<DailyWeight> statisticData)
        {
            return Decode(sms?.Text, out statisticData, sms?.Msisdn);
        }

        /// <summary>
        /// Parse SMS message from GSM scale.
        /// Examples :
        /// SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999
        /// SCALE 123456789012345 DAY 999 31.12.9999 11:59:59PM FEMALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999 MALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999
        /// </summary>
        /// <param name="text"></param>
        /// <param name="statisticData"></param>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        static bool Decode(string text, out List<DailyWeight> statisticData, string phoneNumber)
        {
            statisticData = new List<DailyWeight>();

            if (string.IsNullOrEmpty(text) || string.IsNullOrEmpty(phoneNumber))
            {
                return false;
            }

            var phone = GetValidPhoneNumber(phoneNumber);

            char[] delimeters = { ' ', '\t', '\n' };
            var lenght = text.Trim().Split(delimeters).Length;
            var textBuilder = new StringBuilder(text);
            try
            {
                // Delete SCALES
                DeleteWord(textBuilder, out var str);

                // Delete name value
                DeleteWord(textBuilder, out str);

                // Delete DAY
                DeleteWord(textBuilder, out str);

                // Delete day value
                DeleteWord(textBuilder, out str);
                var day = int.Parse(str, NumberFormatInfo.InvariantInfo);

                // Delete date value
                DeleteWord(textBuilder, out str);
                var date = DateTimeParser.ParseDate(str);

                if (char.IsNumber(textBuilder[0]))
                {
                    // Delete time value
                    DeleteWord(textBuilder, out str);
                    var time = DateTimeParser.ParseTime(str);
                    date = new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, time.Second);
                }

                if (lenght > SHORT_MESSAGE_LENGHT)
                {
                    // Delete FEMALES
                    DeleteWord(textBuilder, out str);
                    statisticData.Add(GetSmsData(textBuilder, SexFemale));

                    //DELETE MALES
                    DeleteWord(textBuilder, out str);
                    statisticData.Add(GetSmsData(textBuilder, SexMale));
                }
                else
                {
                    statisticData.Add(GetSmsData(textBuilder));
                }

                foreach (var statisticSample in statisticData)
                {
                    statisticSample.Day = day;
                    statisticSample.TimeStamp = date;
                    statisticSample.Uid = phone;
                }
                return statisticData.All(CheckDailyWeight);
            }
            catch (Exception)
            {
                return false;
            }
        }

        static string GetValidPhoneNumber(string phoneNumber)
        {
            return $"+{new string(phoneNumber.Where(c => char.IsDigit(c)).ToArray())}";
        }

        static void DeleteWord(StringBuilder text, out string word)
        {
            var spaceIndex = text.ToString().IndexOf(' ', StringComparison.InvariantCulture);
            if (spaceIndex == 0)
            {
                word = text.ToString();
                throw new ArgumentException("Text do not contain space");
            }
            word = text.ToString().Substring(0, spaceIndex);
            text.Remove(0, spaceIndex + 1);
        }

        static DailyWeight GetSmsData(StringBuilder textBuilder, string sex = SexUndefined)
        {
            var culture = CheckLanguage(textBuilder.ToString());
            var statData = new DailyWeight();

            // Cnt
            DeleteWord(textBuilder, out var str);
            DeleteWord(textBuilder, out str);
            statData.Count = int.Parse(str, culture);

            // Avg
            DeleteWord(textBuilder, out str);
            DeleteWord(textBuilder, out str);
            statData.Average = double.Parse(str, culture);

            // Gain
            DeleteWord(textBuilder, out str);
            DeleteWord(textBuilder, out str);
            statData.Gain = double.Parse(str, culture);

            // Sigma
            DeleteWord(textBuilder, out str);
            DeleteWord(textBuilder, out str);
            statData.Sigma = double.Parse(str, culture);

            // CV vypoctu na desetinu (v SMS je to jen na cele cislo)
            DeleteWord(textBuilder, out str);
            DeleteWord(textBuilder, out str);
            statData.Cv = double.Parse(str, culture);

            // Uni
            DeleteWord(textBuilder, out str);
            if (textBuilder.ToString().Split(' ').Length > 1)
            {
                DeleteWord(textBuilder, out str);
                statData.Uniformity = int.Parse(str, culture);
            }
            else
            {
                statData.Uniformity = int.Parse(textBuilder.ToString(), culture);
            }
            statData.Sex = sex;
            return statData;
        }

        static CultureInfo CheckLanguage(string number)
        {
            return new CultureInfo(number.Contains(',', StringComparison.InvariantCulture) ? "de-DE" : "en-US");
        }

        static bool CheckDailyWeight(DailyWeight data)
        {
            if (data.Day < 0 || data.Average < 0 || data.Count < 0 || data.Cv < 0 || data.Sigma < 0 || data.Uniformity < 0)
            {
                return false;
            }
            return true;
        }
    }
}
