﻿using Microsoft.Extensions.Configuration;
using SensorsApi.Filters;
using SensorsApi.Models;

namespace SensorsApi.Extensions
{
    public static class ConfigurationExtensions
    {
        public static string GetKafkaBroker(this IConfiguration configuration)
        {
            return configuration["Kafka"];
        }

        public static string TokenIssuer(this IConfiguration configuration)
        {
            return configuration.GetSection("Token")["Issuer"];
        }

        public static string TokenScopes(this IConfiguration configuration)
        {
            return configuration.GetSection("Token")["Scopes"];
        }

        public static IPWhitelistConfiguration GetIPWhitelist(this IConfiguration configuration)
        {
            return configuration.GetSection("IPAddressWhitelist").Get<IPWhitelistConfiguration>();
        }

        public static string SwaggerUIFile(this IConfiguration configuration)
        {
            return configuration["SwaggerUI"];
        }

        public static HeartbeatSettings GetHeartbeatSettings(this IConfiguration configuration)
        {
            return configuration.GetSection("HeartbeatSetting").Get<HeartbeatSettings>();
        }
    }
}
