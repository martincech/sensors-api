﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SensorsApi.Extensions
{
    public static class DateTimeParser
    {
        /// <summary>
        /// List of date formats
        /// </summary>
        static readonly List<string> dateFormats = new List<string>
        {
           "dd.MM.yyyy",    "dd.M.yyyy",    "d.MM.yyyy",    "d.M.yyyy",
           "MM.dd.yyyy",    "MM.d.yyyy",    "M.dd.yyyy",    "M.d.yyyy",
           "yyyy.MM.dd",    "yyyy.MM.d",    "yyyy.M.dd",    "yyyy.M.d",
           "yyyy.dd.MM",    "yyyy.dd.M",    "yyyy.d.MM",    "yyyy.d.M",
           // short year version
           "dd.MM.yy",      "dd.M.yy",      "d.MM.yy",      "d.M.yy",
           "MM.dd.yy",      "MM.d.yy",      "M.dd.yy",      "M.d.yy",
           "yy.MM.dd",      "yy.MM.d",      "yy.M.dd",      "yy.M.d",
           "yy.dd.MM",      "yy.dd.M",      "yy.d.MM",      "yy.d.M",
           // months as shortcut
           "dd.MMM.yyyy",   "d.MMM.yyyy",
           "MMM.dd.yyyy",   "MMM.d.yyyy",
           "yyyy.MMM.dd",   "yyyy.MMM.d",
           "yyyy.dd.MMM",   "yyyy.d.MMM",
           // short year version
           "dd.MMM.yy",     "d.MMM.yy",
           "MMM.dd.yy",     "MMM.d.yy",
           "yy.MMM.dd",     "yy.MMM.d",
           "yy.dd.MMM",     "yy.d.MMM"
        };

        /// <summary>
        /// List of time formats
        /// </summary>
        static readonly List<string> timeFormats = new List<string>
        {
           "hh.mm.ss",
           "hh.mm.s",
           "hh.m.ss",
           "hh.m.s",
           "h.mm.ss",
           "h.mm.s",
           "h.m.ss",
           "h.m.s"
        };

        /// <summary>
        /// Parse time with default formats
        /// </summary>
        /// <param name="str">string with date</param>
        /// <returns>parsed date</returns>
        public static DateTime ParseTime(string str)
        {
            return ParseTime(str, timeFormats);
        }

        /// <summary>
        /// Parse time using own collection of time formats
        /// </summary>
        /// <param name="str">string with time</param>
        /// <param name="timeFormats"></param>
        /// <returns></returns>
        public static DateTime ParseTime(string str, List<string> timeFormats)
        {
            // get last two chars of string
            var lastTwo = str.ToUpperInvariant().Substring(str.Length - 2);

            // replace original separators with
            for (var i = 0; i < str.Length; i++)
            {
                if (!char.IsNumber(str[i]))
                {
                    str = string.Concat(str.Select((c, pos) => pos == i ? '.' : c));
                }
            }
            // check last two chars
            if (lastTwo == "PM" || lastTwo == "AM")
            {
                str = str.Insert(str.Length - 2, " ");
                str = str.Replace("..", lastTwo, StringComparison.InvariantCulture);

                var modTimeFormats = new List<string>();
                foreach (var format in timeFormats)
                {
                    modTimeFormats.Add(format + " tt");
                }
                timeFormats = modTimeFormats;
            }
            else
            {
                timeFormats = timeFormats.Select((text) => { return text.Replace("h", "H", StringComparison.InvariantCulture); }).ToList();
            }

            DateTime time;
            try
            {
                time = DateTime.ParseExact(str, timeFormats.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
            catch (Exception)
            {
                return DateTime.Now;
            }

            return time;
        }

        /// <summary>
        /// Parse date with default fromats
        /// </summary>
        /// <param name="str">string with date</param>
        /// <returns></returns>
        public static DateTime ParseDate(string str)
        {
            return ParseDate(str, dateFormats);
        }

        /// <summary>
        /// Parse date using own collection of date formats
        /// </summary>
        /// <param name="str">string with date</param>
        /// <param name="dateFormats"></param>
        /// <returns></returns>
        public static DateTime ParseDate(string str, List<string> dateFormats)
        {
            var separators = new List<int>();
            for (var i = 0; i < str.Length; i++)
            {
                if (!char.IsLetterOrDigit(str[i]))
                {
                    str = string.Concat(str.Select((c, pos) => pos == i ? '.' : c));
                }
            }
            DateTime date;
            try
            {
                date = DateTime.ParseExact(str, dateFormats.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
            catch (Exception)
            {
                return DateTime.Now;
            }
            return date;
        }
    }
}
