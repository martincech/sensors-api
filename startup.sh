#!/bin/bash -xe
for f in *.in; do cat $f | envsubst '${VERSION},${ENVIRONMENT},${KAFKA},${COGNITO_ISSUER},${SENTRY_DSN},${INFLUX},${INFLUX_HEARTBEAT_DB},${IAM_ID},${IAM_SECRET}' > `basename $f .in`; done

cp nlog.${ENVIRONMENT}.config nlog.config
exec dotnet SensorsApi.dll

