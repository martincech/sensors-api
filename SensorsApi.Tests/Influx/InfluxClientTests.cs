﻿using System;
using System.Threading.Tasks;
using Heartbeat;
using InfluxData.Net.InfluxDb;
using InfluxData.Net.InfluxDb.ClientModules;
using InfluxData.Net.InfluxDb.Models;
using Moq;
using SensorsApi.Influx;
using Xunit;

namespace SensorsApi.Tests.Influx
{
    public class InfluxClientTests
    {
        const string DATABASE_NAME = "heartbeat";

        readonly InfluxClient influxClient;
        readonly Mock<IInfluxDbClient> mockInfluxDbClient;
        readonly Mock<IDatabaseClientModule> mockDatabaseClientModule;
        readonly Mock<IBasicClientModule> mockBasicClientModule;

        public InfluxClientTests()
        {
            mockDatabaseClientModule = new Mock<IDatabaseClientModule>();
            mockBasicClientModule = new Mock<IBasicClientModule>();
            mockInfluxDbClient = new Mock<IInfluxDbClient>();
            mockInfluxDbClient.SetupGet(s => s.Database).Returns(mockDatabaseClientModule.Object);
            mockInfluxDbClient.SetupGet(s => s.Client).Returns(mockBasicClientModule.Object);

            influxClient = new InfluxClient(mockInfluxDbClient.Object, DATABASE_NAME);
        }

        [Fact]
        public void CreateInstance_WhenDatabaseNameIsNull()
        {
            void act() { var x = new InfluxClient(mockInfluxDbClient.Object, null); }

            Assert.Throws<ArgumentException>(() => act());
        }

        [Fact]
        public void CreateInstance_WhenInfluxDbClientIsNull()
        {
            void act() { var x = new InfluxClient(null, DATABASE_NAME); }

            Assert.Throws<ArgumentNullException>(() => act());
        }

        [Fact]
        public async Task CreateDatabaseAsync()
        {
            await influxClient.CreateDatabaseAsync().ConfigureAwait(false);

            mockDatabaseClientModule
                .Verify(v => v.CreateDatabaseAsync(It.Is<string>(i => i == DATABASE_NAME)), Times.Once);
        }

        [Fact]
        public async Task SaveUserInfoAsync_WhenUserInfoIsNullAsync()
        {
            await influxClient.SaveUserInfoAsync(null).ConfigureAwait(false);

            mockBasicClientModule
              .Verify(v => v.WriteAsync(
                  It.IsAny<Point>(),
                  It.Is<string>(i => i == DATABASE_NAME),
                  It.IsAny<string>(),
                  It.IsAny<string>()),
                  Times.Never);
        }

        [Fact]
        public async Task SaveUserInfoAsync_WhenUserInfoIsNotNullAsync()
        {
            var userInfo = new UserInfo
            {
                OS = new OSInfo
                {
                    Name = "Windows 10",
                    Version = "10.0.0.1",
                    Edition = "Professional",
                    Localization = "CZ"
                },
                App = new AppInfo
                {
                    Id = "BAT Collector",
                    Version = "6.0.0",
                    Localization = "CZ"
                }
            };

            await influxClient.SaveUserInfoAsync(userInfo).ConfigureAwait(false);

            mockBasicClientModule
              .Verify(v => v.WriteAsync(
                  It.Is<Point>(p => VerifyMapping(userInfo, p)),
                  It.Is<string>(i => i == DATABASE_NAME),
                  It.IsAny<string>(),
                  It.IsAny<string>()),
                  Times.Once);

        }

        static bool VerifyMapping(UserInfo expected, Point actual)
        {
            Assert.Equal(expected.App.Id, actual.Fields["AppId"]);
            Assert.Equal(expected.App.Version, actual.Tags["AppVersion"]);
            Assert.Equal(expected.App.Localization, actual.Tags["AppLocalization"]);
            Assert.Equal(expected.OS.Name, actual.Tags["OsName"]);
            Assert.Equal(expected.OS.Edition, actual.Tags["OsEdition"]);
            Assert.Equal(expected.OS.Version, actual.Tags["OsVersion"]);
            Assert.Equal(expected.OS.Localization, actual.Tags["OsLocalization"]);
            return true;
        }
    }
}
