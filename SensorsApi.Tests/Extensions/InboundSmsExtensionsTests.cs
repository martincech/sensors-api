﻿using System;
using System.Globalization;
using System.Linq;
using SensorsApi.Extensions;
using SensorsApi.Models;
using Veit.Kafka.Topics;
using Xunit;

namespace SensorsApi.Tests.Extensions
{
    public class InboundSmsExtensionsTests
    {
        #region Private fields

        readonly string scaleName;
        readonly string phoneNumber;
        readonly int day;
        DateTimeOffset timeStamp;
        DailyWeight femaleStats;
        DailyWeight maleStats;

        #endregion

        #region Test data Initialization

        public InboundSmsExtensionsTests()
        {
            day = 999;
            scaleName = "123456789012345";
            phoneNumber = "420123456789";
            timeStamp = new DateTime(9999, 12, 31, 23, 59, 59);

            femaleStats = new DailyWeight
            {
                Day = day,
                Count = 9999,
                Average = 99.999,
                Gain = -99.999,
                Sigma = 9.999,
                Cv = 999,
                Uniformity = 999,
                Uid = $"+{phoneNumber}",
                TimeStamp = timeStamp,
                Sex = "female",
            };

            maleStats = new DailyWeight
            {
                Day = day,
                Count = 9998,
                Average = 99.998,
                Gain = -99.998,
                Sigma = 9.998,
                Cv = 998,
                Uniformity = 998,
                Uid = $"+{phoneNumber}",
                TimeStamp = timeStamp,
                Sex = "male",
            };
        }

        #endregion

        [Fact]
        [UseCulture("de-DE")]
        public void Decode_Valid_OnlyFemaleData()
        {
            //SCALE 123456789012345 DAY 999 31.12.9999 23:59:59 Cnt 9999 Avg 99,999 Gain -99,999 Sig 9,999 Cv 999 Uni 999
            var message = $"SCALE {scaleName} DAY {day} {timeStamp.ToString("dd/M/yyyy H:mm:ss", CultureInfo.InvariantCulture)} Cnt {femaleStats.Count} Avg {femaleStats.Average} Gain {femaleStats.Gain} Sig {femaleStats.Sigma} Cv {femaleStats.Cv} Uni {femaleStats.Uniformity}";
            var inboundSms = new InboundSms { Text = message, Msisdn = phoneNumber };

            var result = inboundSms.Map(out var data);

            Assert.True(result);
            Assert.Single(data);
            CheckStatData(femaleStats, data.First());
        }

        [Fact]
        [UseCulture("en-US")]
        public void Decode_Valid_BothGenders()
        {
            // SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM FEMALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999 MALES: Cnt 9998 Avg 99.998 Gain -99.998 Sig 9.998 Cv 998 Uni 998
            var message = $"SCALE {scaleName} DAY {day} {timeStamp.ToString("dd/M/yyyy h:mm:sstt", CultureInfo.InvariantCulture)} FEMALES: Cnt {femaleStats.Count} Avg {femaleStats.Average} Gain {femaleStats.Gain} Sig {femaleStats.Sigma} Cv {femaleStats.Cv} Uni {femaleStats.Uniformity} MALES: Cnt {maleStats.Count} Avg {maleStats.Average} Gain {maleStats.Gain} Sig {maleStats.Sigma} Cv {maleStats.Cv} Uni {maleStats.Uniformity}";
            var inboundSms = new InboundSms { Text = message, Msisdn = phoneNumber };

            var result = inboundSms.Map(out var data);

            Assert.True(result);
            Assert.Equal(2, data.Count);
            CheckStatData(femaleStats, data.First());
            CheckStatData(maleStats, data.Last());
        }

        [Fact]
        public void Decode_NotValid()
        {
            CheckNotValidSms("ABC");
            CheckNotValidSms("");
            CheckNotValidSms("SCALE 123456 31.12.9999 23:59:59");                            //missing statistic data
            CheckNotValidSms("Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999");   //missing header
        }


        #region Private helpers

        private static void CheckNotValidSms(string text)
        {
            var inboundSms = new InboundSms { Text = text, Msisdn = "123456789" };
            var result = inboundSms.Map(out var sample);
            Assert.False(result);
        }

        private static void CheckStatData(DailyWeight expected, DailyWeight actual)
        {
            Assert.NotNull(actual);
            Assert.Equal(expected.Uid, actual.Uid);
            Assert.Equal(expected.Day, actual.Day);
            Assert.True(DateTimeOffset.Compare(expected.TimeStamp, actual.TimeStamp) == 0);
            Assert.Equal(expected.Count, actual.Count);
            Assert.Equal(expected.Average, actual.Average);
            Assert.Equal(expected.Gain, actual.Gain);
            Assert.Equal(expected.Sigma, actual.Sigma);
            Assert.Equal(expected.Cv, actual.Cv);
            Assert.Equal(expected.Uniformity, actual.Uniformity);
        }
        #endregion
    }
}
