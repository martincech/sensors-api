﻿using System.Collections.Generic;
using System.Security.Claims;
using SensorsApi.Extensions;
using Xunit;

namespace SensorsApi.Tests.Extensions
{
    public class ClaimValidationExtensionsTests
    {
        [Fact]
        public void HasValidScope_WhenRequiredScopeIsNull()
        {
            Assert.True(ClaimValidationExtensions.HasValidScope(null, null));
        }

        [Fact]
        public void HasValidScope_WhenRequiredScopeIsEmpty()
        {
            Assert.True(ClaimValidationExtensions.HasValidScope(null, ""));
        }

        [Fact]
        public void HasValidScope_WhenClaimsIsNull()
        {
            Assert.False(ClaimValidationExtensions.HasValidScope(null, "test"));
        }

        [Fact]
        public void HasValidScope_WhenClaimsIsEmpty()
        {
            var claims = new List<Claim>();
            Assert.False(claims.HasValidScope("test"));
        }

        [Fact]
        public void HasValidScope_WhenClaimsDoNotContainRequiredScope()
        {
            var claims = new List<Claim> { new Claim("scope", "test") };

            Assert.False(claims.HasValidScope("test2"));
        }

        [Fact]
        public void HasValidScope_WhenClaimsContainRequiredScope()
        {
            const string requiredScope = "test";
            var claims = new List<Claim> { new Claim("scope", requiredScope) };

            Assert.True(claims.HasValidScope(requiredScope));
        }

        [Fact]
        public void HasValidScope_WhenClaimsMissingOneRequiredScope()
        {
            const string requiredScope1 = "test";
            const string requiredScope2 = "test2";
            var claims = new List<Claim> { new Claim("scope", requiredScope1) };

            Assert.False(claims.HasValidScope($"{requiredScope1} {requiredScope2}"));
        }

        [Fact]
        public void HasValidScope_WhenClaimsContainMultipleRequiredScopes()
        {
            const string requiredScope1 = "test";
            const string requiredScope2 = "test2";
            var claims = new List<Claim> {
                new Claim("scope", $"{requiredScope2} {requiredScope1}"),
            };

            Assert.True(claims.HasValidScope($"{requiredScope1} {requiredScope2}"));
        }
    }
}
