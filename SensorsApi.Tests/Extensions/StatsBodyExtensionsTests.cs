﻿using System;
using System.Collections.Generic;
using System.Linq;
using SensorsApi.Extensions;
using Veit.Bat.Common.Schema;
using Veit.Kafka.Topics;
using Xunit;

namespace SensorsApi.Tests.Extensions
{
    public class StatsBodyExtensionsTests
    {
        static Guid id = Guid.NewGuid();
        static readonly Random rnd = new Random();

        [Fact]
        public void Map_WhenStatsBodyIsNull()
        {
            var result = StatsBodyExtensions.Map(null);

            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public void Map_WhenScalesIsNull()
        {
            var statsBody = new StatsBody();

            var result = statsBody.Map();

            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public void Map_WhenFlockIsNull()
        {
            var statsBody = new StatsBody
            {
                Scales = new List<Scale> {
                    new Scale()
                }
            };

            var result = statsBody.Map();

            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public void Map_WhenSexStatsAreNull()
        {
            var statsBody = new StatsBody
            {
                Scales = new List<Scale> {
                    new Scale {
                        Flock = new Flock()
                    }
                }
            };

            var result = statsBody.Map();

            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public void Map_WhenSexStatsMaleOnly()
        {
            var stats = CreateSexStat(2);
            var statsBody = CreateStatsBody(stats, null);

            var result = statsBody.Map();

            ValidateResult(result, stats);
        }

        [Fact]
        public void Map_WhenSexStatsFemaleOnly()
        {
            var stats = CreateSexStat(2);
            var statsBody = CreateStatsBody(null, stats);

            var result = statsBody.Map();

            ValidateResult(result, stats);
        }

        [Fact]
        public void Map_WhenSexStatsMaleAndFemale()
        {
            var maleStats = CreateSexStat(2);
            var femaleStats = CreateSexStat(2);
            var statsBody = CreateStatsBody(maleStats, femaleStats);

            var result = statsBody.Map();

            ValidateResult(result, null, maleStats, femaleStats);
        }

        static void ValidateResult(List<DailyWeight> actual, SexStats expectedUndefined = null,
            SexStats expectedMale = null, SexStats expectedFemale = null)
        {

            var actualMale = actual.Where(w => w.Sex == StatsBodyExtensions.SexMale);
            var actualFemale = actual.Where(w => w.Sex == StatsBodyExtensions.SexFemale);
            var actualUndefined = actual.Where(w => w.Sex == StatsBodyExtensions.SexUndefined);

            var expectedMaleCount = expectedMale?.Stats?.Count ?? 0;
            var expectedFemaleCount = expectedFemale?.Stats?.Count ?? 0;
            var expectedUndefinedCount = expectedUndefined?.Stats?.Count ?? 0;

            var total = expectedMaleCount + expectedFemaleCount + expectedUndefinedCount;

            Assert.Equal(total, actual.Count);
            Assert.Equal(expectedMaleCount, actualMale.Count());
            Assert.Equal(expectedFemaleCount, actualFemale.Count());
            Assert.Equal(expectedUndefinedCount, actualUndefined.Count());

            ValidateStats(expectedMale, actualMale, StatsBodyExtensions.SexMale);
            ValidateStats(expectedFemale, actualFemale, StatsBodyExtensions.SexFemale);
            ValidateStats(expectedUndefined, actualUndefined, StatsBodyExtensions.SexUndefined);
        }

        static void ValidateStats(SexStats expected, IEnumerable<DailyWeight> actual, string sex)
        {
            Assert.Equal(expected?.Stats == null, !actual.Any());
            if (expected?.Stats != null)
            {
                foreach (var expextedStats in expected.Stats)
                {
                    var actualStats = actual.FirstOrDefault(f => f.Day == expextedStats.Day);
                    ValidateValues(expextedStats, actualStats, sex);
                }
            }
        }

        static void ValidateValues(DailyStats expected, DailyWeight actual, string sex)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.Equal(id.ToString(), actual.Uid);
            Assert.Equal(sex, actual.Sex);
            Assert.Equal(expected.Avg, actual.Average);
            Assert.Equal(expected.Count, actual.Count);
            Assert.Equal(expected.Cv, actual.Cv);
            Assert.Equal(expected.DateTime.DateTime.Ticks, actual.TimeStamp.Ticks);
            Assert.Equal(expected.Day, actual.Day);
            Assert.Equal(expected.Gain, actual.Gain);
            Assert.Equal(expected.Sig, actual.Sigma);
            Assert.Equal(expected.Uniformity, actual.Uniformity);
        }

        static StatsBody CreateStatsBody(SexStats male, SexStats female)
        {
            return new StatsBody
            {
                Scales = new List<Scale> {
                    new Scale {
                        Id = id,
                        Day = rnd.Next(1,100),
                        Flock = new Flock
                        {
                            Males = male,
                            Females = female,
                        }
                    }
                }
            };
        }

        static SexStats CreateSexStat(int dailyStatsCount = 1)
        {
            return new SexStats
            {
                Stats = Enumerable.Range(0, dailyStatsCount).Select(s => new DailyStats
                {
                    Avg = rnd.Next(),
                    Count = rnd.Next(),
                    Cv = rnd.Next(),
                    DateTime = DateTimeOffset.Now,
                    Day = s,
                    Gain = rnd.Next(),
                    Sig = rnd.Next(),
                    Uniformity = rnd.Next(),
                }).ToList()
            };
        }
    }
}
